import torch
import torch.nn as nn

class GMidAttIndex (nn.Module):

    def __init__(self,in_channels = 1, out_channels = 1):
        '''
        initialize the unet
        '''
        super(GMidAttIndex, self).__init__()
        ## con0_conv1_pool1
        self.encode1 = nn.Sequential(
            nn.Conv2d(in_channels,48,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(48,48,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.MaxPool2d(2))
        ##conv2_pool2
        self.encode2 = nn.Sequential(
            nn.Conv2d(48,48,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.MaxPool2d(2))
        ##conv3_pool3
        self.encode3 = nn.Sequential(
            nn.Conv2d(48,48,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.MaxPool2d(2))
        ##conv4_pool4
        self.encode4 = nn.Sequential(
            nn.Conv2d(48,48,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.MaxPool2d(2))
        ##conv5_pool5
        self.encode5 = nn.Sequential(
            nn.Conv2d(48,48,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.MaxPool2d(2))
        ##conv6_upsample5
        self.encode6 = nn.Sequential(
            nn.Conv2d(48,48,3,stride=1,padding=1),
            nn.ConvTranspose2d(48, 48, 3, stride=2, padding=1, output_padding=1))
        ## decon5a_b_upsample4
        self.decode1 = nn.Sequential(
            nn.Conv2d(96,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(96,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.ConvTranspose2d(96, 96, 3, stride=2, padding=1, output_padding=1))
        ## deconv4a_4b_upsample3
        self.decode2 = nn.Sequential(
            nn.Conv2d(144,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(96,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.ConvTranspose2d(96, 96, 3, stride=2, padding=1, output_padding=1))
        ##  deconv3a_3b_upsample2
        self.decode3 = nn.Sequential(
            nn.Conv2d(144,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(96,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.ConvTranspose2d(96, 96, 3, stride=2, padding=1, output_padding=1))
        ##  deconv2a_2b_upsample1
        self.decode4 = nn.Sequential(
            nn.Conv2d(144,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(96,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.ConvTranspose2d(96, 96, 3, stride=2, padding=1, output_padding=1))
        ## deconv1a_1b
        self.decode5 = nn.Sequential (
            nn.Conv2d(96 + in_channels,64,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(64,32,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1))
        ## output layer
        self.output_layer = nn.Conv2d(32,out_channels,3,stride=1,padding=1)

        ##Distill Branch
        self.D_encode6 = nn.Sequential(
            nn.Conv2d(48,48,3,stride=1,padding=1),
            nn.ConvTranspose2d(48, 48, 3, stride=2, padding=1, output_padding=1))
        ## decon5a_b_upsample4
        self.D_decode1 = nn.Sequential(
            nn.Conv2d(96,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(96,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.ConvTranspose2d(96, 96, 3, stride=2, padding=1, output_padding=1))
        ## deconv4a_4b_upsample3
        self.D_decode2 = nn.Sequential(
            nn.Conv2d(144,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(96,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.ConvTranspose2d(96, 96, 3, stride=2, padding=1, output_padding=1))
        ##  deconv3a_3b_upsample2
        self.D_decode3 = nn.Sequential(
            nn.Conv2d(144,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(96,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.ConvTranspose2d(96, 96, 3, stride=2, padding=1, output_padding=1))
        ##  deconv2a_2b_upsample1
        self.D_decode4 = nn.Sequential(
            nn.Conv2d(144,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(96,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.ConvTranspose2d(96, 96, 3, stride=2, padding=1, output_padding=1))
        ## deconv1a_1b
        self.D_decode5 = nn.Sequential (
            nn.Conv2d(96 + in_channels,64,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(64,32,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1))
        ## output layer
        self.D_output_layer = nn.Conv2d(32,out_channels,3,stride=1,padding=1)

        #Attention module
        Att_in=144
        self.midn=72
        self.Att_convDist=nn.Conv2d(Att_in,self.midn,1)
        self.Att_convSelf1=nn.Conv2d(Att_in,self.midn,1)
        self.Att_convSelf2=nn.Conv2d(Att_in,self.midn,1)
        self.Att_convCom=nn.Conv2d(self.midn,Att_in,1)
        self.Att_BNDist=nn.BatchNorm2d(self.midn)
        self.Att_BNSelf1=nn.BatchNorm2d(self.midn)
        self.Att_BNSelf2=nn.BatchNorm2d(self.midn)
        self.Att_BNCom=nn.BatchNorm2d(Att_in)

        ##Index Branch
        self.I_encode6 = nn.Sequential(nn.Conv2d(48,48,3,stride=1,padding=1),nn.ReLU(inplace=True))
        self.I_decode1 = nn.Sequential(nn.Conv2d(48,32,3,stride=1,padding=0),nn.ReLU(inplace=True))
        self.I_decode2 = nn.Sequential(nn.Conv2d(32,16,3,stride=1,padding=0),nn.ReLU(inplace=True))
        self.I_decode3 = nn.Sequential(nn.Conv2d(16,4,3,stride=1,padding=0),nn.ReLU(inplace=True))
        ## output layer
        self.I_output_layer = nn.Linear(16,2)

        ## initialize weight
        self._init_weights()

    def forward(self,x):
        '''
        forward function
        '''
        batch_size=x.size(0)

        pool1 = self.encode1(x)
        pool2 = self.encode2(pool1)
        pool3 = self.encode3(pool2)
        pool4 = self.encode4(pool3)
        pool5 = self.encode5(pool4)

        upsample5 = self.encode6(pool5)
        concat5 = torch.cat((upsample5,pool4),dim=1)
        upsample4 = self.decode1(concat5)
        concat4 = torch.cat((upsample4,pool3),dim=1)
        upsample3 = self.decode2(concat4)
        # concat3 = torch.cat((upsample3,pool2),dim=1)
        self_out_feat = torch.cat((upsample3,pool2),dim=1)
        upsample2 = self.decode3(self_out_feat)
        concat2 = torch.cat((upsample2,pool1),dim=1)
        upsample1 = self.decode4(concat2)
        concat1 = torch.cat((upsample1,x),dim =1)
        umsample0 = self.decode5(concat1)
        output = self.output_layer(umsample0)

        #Distillation
        D_upsample5 = self.D_encode6(pool5)
        D_concat5 = torch.cat((D_upsample5,pool4),dim=1)
        D_upsample4 = self.D_decode1(D_concat5)
        D_concat4 = torch.cat((D_upsample4,pool3),dim=1)
        D_upsample3 = self.D_decode2(D_concat4)
        D_out_feat = torch.cat((D_upsample3,pool2),dim=1)

        #Attention
        Self1 = self.Att_BNSelf1(self.Att_convSelf1(self_out_feat))
        Self2 = self.Att_BNSelf2(self.Att_convSelf2(self_out_feat))
        theta_d = Self1.view(batch_size, self.midn, -1)
        theta_d = theta_d.permute(0, 2, 1)
        phi_d = Self2.view(batch_size, self.midn, -1)
        SelfGuid = torch.matmul(theta_d, phi_d)
        SelfGuid = nn.functional.softmax(SelfGuid, dim=-1)
        Dist = self.Att_BNDist(self.Att_convDist(D_out_feat))
        g_s = Dist.view(batch_size, self.midn, -1)
        g_s = g_s.permute(0, 2, 1)
        Com = torch.matmul(SelfGuid, g_s)
        Com = Com.permute(0, 2, 1).contiguous()
        Com = Com.view(batch_size, self.midn, *D_out_feat.size()[2:])
        Att_D = self.Att_BNCom(self.Att_convCom(Com)) + D_out_feat

        D_upsample2 = self.D_decode3(Att_D)
        D_concat2 = torch.cat((D_upsample2,pool1),dim=1)
        D_upsample1 = self.D_decode4(D_concat2)
        D_concat1 = torch.cat((D_upsample1,x),dim =1)
        D_umsample0 = self.D_decode5(D_concat1)
        D_output = self.D_output_layer(D_umsample0)

        I_1=self.I_encode6(pool5)
        I_2=self.I_decode1(I_1)
        I_3=self.I_decode2(I_2)
        I_4=self.I_decode3(I_3)
        I_4=I_4.view(I_4.size(0),-1)
        I_out=self.I_output_layer(I_4)

        return output, D_output, pool5,I_out

    def _init_weights(self):
        """Initializes weights using He et al. (2015)."""

        for m in self.modules():
            if isinstance(m, nn.ConvTranspose2d) or isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight.data)
                nn.init.constant_(m.bias.data, 0)

class GBackMid (nn.Module):

    def __init__(self,in_channels = 1, out_channels = 1):
        '''
        initialize the unet
        '''
        super(GBackMid, self).__init__()
        ## con0_conv1_pool1
        self.encode1 = nn.Sequential(
            nn.Conv2d(in_channels,48,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(48,48,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.MaxPool2d(2))
        ##conv2_pool2
        self.encode2 = nn.Sequential(
            nn.Conv2d(48,48,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.MaxPool2d(2))
        ##conv3_pool3
        self.encode3 = nn.Sequential(
            nn.Conv2d(48,48,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.MaxPool2d(2))
        ##conv4_pool4
        self.encode4 = nn.Sequential(
            nn.Conv2d(48,48,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.MaxPool2d(2))
        ##conv5_pool5
        self.encode5 = nn.Sequential(
            nn.Conv2d(48,48,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.MaxPool2d(2))
        ##conv6_upsample5
        self.encode6 = nn.Sequential(
            nn.Conv2d(48,48,3,stride=1,padding=1),
            nn.ConvTranspose2d(48, 48, 3, stride=2, padding=1, output_padding=1))
        ## decon5a_b_upsample4
        self.decode1 = nn.Sequential(
            nn.Conv2d(96,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(96,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.ConvTranspose2d(96, 96, 3, stride=2, padding=1, output_padding=1))
        ## deconv4a_4b_upsample3
        self.decode2 = nn.Sequential(
            nn.Conv2d(144,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(96,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.ConvTranspose2d(96, 96, 3, stride=2, padding=1, output_padding=1))
        ##  deconv3a_3b_upsample2
        self.decode3 = nn.Sequential(
            nn.Conv2d(144,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(96,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.ConvTranspose2d(96, 96, 3, stride=2, padding=1, output_padding=1))
        ##  deconv2a_2b_upsample1
        self.decode4 = nn.Sequential(
            nn.Conv2d(144,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(96,96,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.ConvTranspose2d(96, 96, 3, stride=2, padding=1, output_padding=1))
        ## deconv1a_1b
        self.decode5 = nn.Sequential (
            nn.Conv2d(96 + in_channels,64,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1),
            nn.Conv2d(64,32,3,stride=1,padding=1),
            nn.LeakyReLU(negative_slope = 0.1))
        ## output layer
        self.output_layer = nn.Conv2d(32,out_channels,3,stride=1,padding=1)

        ## initialize weight
        self._init_weights()

    def forward(self,x):
        '''
        forward function
        '''
        pool1 = self.encode1(x)
        pool2 = self.encode2(pool1)
        pool3 = self.encode3(pool2)
        pool4 = self.encode4(pool3)
        pool5 = self.encode5(pool4)

        upsample5 = self.encode6(pool5)
        concat5 = torch.cat((upsample5,pool4),dim=1)
        upsample4 = self.decode1(concat5)
        concat4 = torch.cat((upsample4,pool3),dim=1)
        upsample3 = self.decode2(concat4)
        concat3 = torch.cat((upsample3,pool2),dim=1)
        upsample2 = self.decode3(concat3)
        concat2 = torch.cat((upsample2,pool1),dim=1)
        upsample1 = self.decode4(concat2)
        concat1 = torch.cat((upsample1,x),dim =1)
        umsample0 = self.decode5(concat1)
        output = self.output_layer(umsample0)

        return output,pool5

    def _init_weights(self):
        """Initializes weights using He et al. (2015)."""

        for m in self.modules():
            if isinstance(m, nn.ConvTranspose2d) or isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight.data)
                nn.init.constant_(m.bias.data, 0)

class DNet (nn.Module):

    def __init__(self,in_channels = 1):
        '''
        initialize the unet
        '''
        super(DNet, self).__init__()

        def discriminator_block(in_filt,out_filt,stride,normalize):
            layers=[nn.Conv2d(in_filt,out_filt,3,stride,1)]
            if normalize:
                layers.append(nn.InstanceNorm2d(out_filt))
            layers.append(nn.LeakyReLU(0.2,inplace=True))
            return layers

        layers = []
        in_filt=in_channels
        for out_filt,stride,normalize in [
            (64,2,False),
            (128,2,True),
            (256,2,True),
            (512,1,True)]:
            layers.extend(discriminator_block(in_filt,out_filt,stride,normalize))
            in_filt=out_filt

        layers.append(nn.Conv2d(out_filt,1,3,1,1))
        self.model = nn.Sequential(*layers)

        ## initialize weight
        self._init_weights()

    def forward(self,x):
        '''
        forward function
        '''
        return self.model(x)

    def _init_weights(self):
        """Initializes weights using He et al. (2015)."""

        for m in self.modules():
            if isinstance(m, nn.ConvTranspose2d) or isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight.data)
                nn.init.constant_(m.bias.data, 0)

class DNetBack (nn.Module):

    def __init__(self,in_channels = 1):
        '''
        initialize the unet
        '''
        super(DNetBack, self).__init__()

        def discriminator_block(in_filt,out_filt,stride,normalize):
            layers=[nn.Conv2d(in_filt,out_filt,3,stride,1)]
            if normalize:
                layers.append(nn.InstanceNorm2d(out_filt))
            layers.append(nn.LeakyReLU(0.2,inplace=True))
            return layers

        layers = []
        in_filt=in_channels
        for out_filt,stride,normalize in [
            (64,2,False),
            (128,2,True),
            (256,2,True),
            (512,1,True)]:
            layers.extend(discriminator_block(in_filt,out_filt,stride,normalize))
            in_filt=out_filt

        layers.append(nn.Conv2d(out_filt,1,3,1,1))
        self.model = nn.Sequential(*layers)

        ## initialize weight
        self._init_weights()

    def forward(self,x):
        '''
        forward function
        '''
        return self.model(x)

    def _init_weights(self):
        """Initializes weights using He et al. (2015)."""

        for m in self.modules():
            if isinstance(m, nn.ConvTranspose2d) or isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight.data)
                nn.init.constant_(m.bias.data, 0)

class DNetMid (nn.Module):

    def __init__(self,in_channels = 1):
        '''
        initialize the unet
        '''
        super(DNetMid, self).__init__()

        def discriminator_block(in_filt,out_filt,stride,normalize):
            layers=[nn.Conv2d(in_filt,out_filt,3,stride,1)]
            if normalize:
                layers.append(nn.InstanceNorm2d(out_filt))
            layers.append(nn.LeakyReLU(0.2,inplace=True))
            return layers

        layers = []
        in_filt=in_channels
        for out_filt,stride,normalize in [
            (64,2,False),
            (128,2,True),
            (256,2,True),
            (512,1,True)]:
            layers.extend(discriminator_block(in_filt,out_filt,stride,normalize))
            in_filt=out_filt

        layers.append(nn.Conv2d(out_filt,1,3,1,1))
        self.model = nn.Sequential(*layers)

        ## initialize weight
        self._init_weights()

    def forward(self,x):
        '''
        forward function
        '''
        return self.model(x)

    def _init_weights(self):
        """Initializes weights using He et al. (2015)."""

        for m in self.modules():
            if isinstance(m, nn.ConvTranspose2d) or isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight.data)
                nn.init.constant_(m.bias.data, 0)

class DNetEdge (nn.Module):

    def __init__(self,in_channels = 1):
        '''
        initialize the unet
        '''
        super(DNetEdge, self).__init__()

        def discriminator_block(in_filt,out_filt,stride,normalize):
            layers=[nn.Conv2d(in_filt,out_filt,3,stride,1)]
            if normalize:
                layers.append(nn.InstanceNorm2d(out_filt))
            layers.append(nn.LeakyReLU(0.2,inplace=True))
            return layers

        layers = []
        in_filt=in_channels
        for out_filt,stride,normalize in [
            (64,2,False),
            (128,2,True),
            (256,2,True),
            (512,1,True)]:
            layers.extend(discriminator_block(in_filt,out_filt,stride,normalize))
            in_filt=out_filt

        layers.append(nn.Conv2d(out_filt,1,3,1,1))
        self.model = nn.Sequential(*layers)

        ## initialize weight
        self._init_weights()

    def forward(self,x):
        '''
        forward function
        '''
        return self.model(x)

    def _init_weights(self):
        """Initializes weights using He et al. (2015)."""

        for m in self.modules():
            if isinstance(m, nn.ConvTranspose2d) or isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight.data)
                nn.init.constant_(m.bias.data, 0)
