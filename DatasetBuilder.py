from __future__ import print_function, division
import os
import torch
from skimage import io
from skimage.transform import resize
import numpy as np
import random
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms.functional as tvF


class Training_Dataset(Dataset):

    def __init__(self,image_dir,target_dir,image_size, crop = True, transform=None):
        self.image_dir = image_dir
        self.image_list = os.listdir(image_dir)
        self.target_dir = target_dir
        self.target_list = os.listdir(target_dir)
        self.taglen=len(self.target_list)
        self.image_size = image_size
        self.crop_img = crop
        self.transform=transform
        self.dict={'x':1,'y':2,'z':3}#Index view

    def __len__(self):
        return len(os.listdir(self.image_dir))

    def __getitem__(self,idx):
        image_name = os.path.join(self.image_dir,self.image_list[idx])
        img = io.imread(image_name)
        sp=self.image_list[idx].split('_')#split the file name into different parts to sp, e.g., the file "x_107_87.png" will be split into "x", "107" and "87.png". Please modify the parts with "sp" according to your own data formats.
        index1=float(self.dict[sp[0]])
        index2=float(sp[2][:-4])
        index=np.array([index1, index2])
        img_p_name=os.path.join(self.image_dir,sp[0]+'_'+sp[1]+'_'+str(int(sp[2][:-4])-1)+'.png')#previous slice
        img_n_name=os.path.join(self.image_dir,sp[0]+'_'+sp[1]+'_'+str(int(sp[2][:-4])+1)+'.png')#next slice
        if os.path.isfile(img_p_name):
            img_p=io.imread(img_p_name)
        else:
            img_p=img.copy()
        if os.path.isfile(img_n_name):
            img_n=io.imread(img_n_name)
        else:
            img_n=img.copy()

        if len(img.shape)==2:
            img=np.stack((img,img,img),2)
        if len(img_p.shape)==2:
            img_p=np.stack((img_p,img_p,img_p),2)
        if len(img_n.shape)==2:
            img_n=np.stack((img_n,img_n,img_n),2)
        img_3=np.concatenate((img_p,img,img_n),2)
        target_name = os.path.join(self.target_dir,self.target_list[idx%self.taglen])
        tag = io.imread(target_name)
        if len(tag.shape)==2:
            tag=np.stack((tag,tag,tag),2)

        img_cropped = self.__crop_img(img_3).astype(np.uint8)
        tag_cropped = self.__crop_img(tag).astype(np.uint8)
        if self.transform:
            img_cropped=tvF.to_pil_image(img_cropped)
            img_cropped = self.transform(img_cropped)
            tag_cropped=tvF.to_pil_image(tag_cropped)
            tag_cropped = self.transform(tag_cropped)

        source = tvF.to_tensor(img_cropped)
        target = tvF.to_tensor(tag_cropped)
        index = torch.from_numpy(index)

        return source , target, index

    def __crop_img(self,img):
        '''crop the img '''
        h, w,c = img.shape
        new_h, new_w = self.image_size,self.image_size
        if min(h,w) <  self.image_size:
            img = resize(img,(self.image_size,self.image_size),preserve_range=True, mode='constant')
        h_r,w_r,c = img.shape
        top = np.random.randint(0,h_r-new_h+1)
        left = np.random.randint(0,w_r-new_w+1)
        cropped_img = img[top:top+new_h,left:left+new_w]

        return cropped_img
