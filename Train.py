import torch
import torch.nn as nn
from torch.optim import Adam, lr_scheduler
import os 
import torchvision.transforms as transform
from Models import GMidAttIndex, GBackMid, DNet, DNetBack, DNetMid, DNetEdge
from Config import Config as conf
from DatasetBuilder import Training_Dataset
from torch.utils.data import Dataset, DataLoader
from torch.autograd import Variable
from tensorboardX import SummaryWriter
import torchvision.utils as vutils
import itertools
from net_canny import CannyNet

# os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
# os.environ["CUDA_VISIBLE_DEVICES"] = "1"
torch.backends.cudnn.benchmark=True

setting="ours"
def save_model(model_g,model_d,model_gb,model_db,model_dmid,model_de,epoch):
    '''save model for eval'''

    ckpt_name_g = '/G_{}_epoch_{}.pth'.format(setting,epoch)
    ckpt_name_d = '/D_{}_epoch_{}.pth'.format(setting,epoch)
    ckpt_name_gb = '/GB_{}_epoch_{}.pth'.format(setting,epoch)
    ckpt_name_db = '/DB_{}_epoch_{}.pth'.format(setting,epoch)
    ckpt_name_dmid = '/Dmid{}_epoch_{}.pth'.format(setting,epoch)
    ckpt_name_dE = '/DE_{}_epoch_{}.pth'.format(setting,epoch)
    path = conf.data_path_checkpoint
    if not  os.path.exists(path):
        os.mkdir(path)
    path_final_g = path + ckpt_name_g
    path_final_d = path + ckpt_name_d
    path_final_gb = path + ckpt_name_gb
    path_final_db = path + ckpt_name_db
    path_final_dmid = path + ckpt_name_dmid
    path_final_dE = path + ckpt_name_dE
    print('Saving checkpoint to: {} and {}\n'.format(path_final_g, path_final_d))
    torch.save(model_g.state_dict(), path_final_g)
    torch.save(model_d.state_dict(), path_final_d)
    torch.save(model_gb.state_dict(), path_final_gb)
    torch.save(model_db.state_dict(), path_final_db)
    torch.save(model_dmid.state_dict(), path_final_dmid)
    torch.save(model_de.state_dict(), path_final_dE)

batch_size=12
patch=(1,int(conf.crop_img_size/2**3),int(conf.crop_img_size/2**3))
patch_mid=(1,1,1)

def train():
    device = torch.device(conf.cuda if torch.cuda.is_available() else "cpu")

    writer=SummaryWriter('./result/runs_{}'.format(setting))

    trans=transform.Compose([
        transform.RandomHorizontalFlip()
    ])
    dataset = Training_Dataset(conf.data_path_train,conf.target_path_train,conf.crop_img_size,transform=None)
    dataset_length = len(dataset)
    train_loader = DataLoader(dataset, batch_size=batch_size, shuffle=True,num_workers=8)

    model_G = GMidAttIndex(in_channels =conf.img_channel * 3,out_channels=conf.img_channel)#the forward path with middle latent feature, cross-modal attention and index prediction branch
    model_GB = GBackMid(in_channels =conf.img_channel,out_channels=conf.img_channel)#the backward path with middle latent feature
    model_D = DNet(in_channels=conf.img_channel)#the discriminator for MRI appearance
    model_DB = DNetBack(in_channels=conf.img_channel)#the discriminator for backward US appearance
    model_Dmid = DNetMid(48)#the discriminator for bi-directional latent space
    model_DE = DNetEdge(1)#the discriminator for structure
    criterion_G = nn.L1Loss()
    criterion_D = nn.MSELoss()
    Canny=CannyNet(requires_grad=False,threshold=3.0)

    if torch.cuda.is_available():
        model_G = model_G.to(device)
        model_GB = model_GB.to(device)
        model_D = model_D.to(device)
        model_DB = model_DB.to(device)
        model_Dmid = model_Dmid.to(device)
        model_DE = model_DE.to(device)
        criterion_G=criterion_G.to(device)
        criterion_D=criterion_D.to(device)
        Canny=Canny.to(device)

    optim_G = Adam(itertools.chain(model_G.parameters(),model_GB.parameters()), lr = conf.learning_rate, betas=(0.9, 0.999))
    optim_D = Adam(model_D.parameters(), lr = conf.learning_rate, betas=(0.9, 0.999))
    optim_DB = Adam(model_DB.parameters(), lr = conf.learning_rate, betas=(0.9, 0.999))
    optim_Dmid = Adam(model_Dmid.parameters(), lr = conf.learning_rate, betas=(0.9, 0.999))
    optim_Dedge = Adam(model_DE.parameters(), lr = conf.learning_rate, betas=(0.9, 0.999))

    scheduler_G = lr_scheduler.StepLR(optim_G, step_size=20, gamma=0.5)
    scheduler_D = lr_scheduler.StepLR(optim_D, step_size=20, gamma=0.5)
    scheduler_DB = lr_scheduler.StepLR(optim_DB, step_size=20, gamma=0.5)
    scheduler_Dmid = lr_scheduler.StepLR(optim_Dmid, step_size=20, gamma=0.5)
    scheduler_Dedge = lr_scheduler.StepLR(optim_Dedge, step_size=20, gamma=0.5)

    # model.train()
    print(model_G,model_D)
    print("Starting Training Loop...")
    for epoch in range(conf.max_epoch):
        print('Epoch {}/{}'.format(epoch, conf.max_epoch - 1))
        print('-' * 10)
        running_loss_g_adv = 0.0
        running_loss_g_advB = 0.0
        running_loss_g_advmid = 0.0
        running_loss_g_advedge = 0.0
        running_loss_g_gen = 0.0
        running_loss_g_rec = 0.0
        running_loss_g_ind = 0.0
        running_loss_d = 0.0
        scheduler_G.step()
        scheduler_D.step()
        scheduler_DB.step()
        scheduler_Dmid.step()
        scheduler_Dedge.step()
        for batch_idx , (source_3,target,index) in enumerate(train_loader):

            source_3 = source_3.to(device)#input US with (2-nearest) neighbours
            source = source_3[:,3:6,:,:]#interested center view
            target = target.to(device)#sample from the target MRI dataset
            index = index.to(device)#3D location/index of the center view
            valid=torch.Tensor(source.size(0),*patch).fill_(1.0)
            fake=torch.Tensor(source.size(0),*patch).fill_(0.0)
            valid=valid.to(device)
            fake=fake.to(device)
            valid_mid=torch.Tensor(source.size(0),*patch_mid).fill_(1.0)
            fake_mid=torch.Tensor(source.size(0),*patch_mid).fill_(0.0)
            valid_mid=valid_mid.to(device)
            fake_mid=fake_mid.to(device)

            #Train G net
            optim_G.zero_grad()

            rec, gen, midG, ind = model_G(source_3)
            gen_edge=Canny(gen)
            targ_edge=Canny(target)
            #add mask
            mask=(source!=0).float()
            gen*=mask
            gen_back, midGB=model_GB(gen)
            gen_back*=mask

            g_adv=criterion_D(model_D(gen),valid)
            g_advB=criterion_D(model_DB(gen_back),valid)
            g_advmid=criterion_D(model_Dmid(midGB),valid_mid)
            g_advedge=criterion_D(model_DE(gen_edge.detach()),valid)
            g_genB=criterion_G(gen_back,source)
            g_rec=criterion_G(rec,source)
            g_ind=criterion_G(ind,index.float())
            g_loss=0.69*g_rec+0.69*g_genB+0.01*g_adv+0.01*g_advB+0.01*g_advmid+0.01*g_advedge+0.49*g_ind
            g_loss.backward()
            nn.utils.clip_grad_norm_(model_G.parameters(),2)
            optim_G.step()

            #Train D net
            optim_D.zero_grad()
            optim_DB.zero_grad()
            optim_Dmid.zero_grad()
            optim_Dedge.zero_grad()

            real_loss=criterion_D(model_D(target),valid)
            fake_loss=criterion_D(model_D(gen.detach()),fake)
            real_lossB=criterion_D(model_DB(source),valid)
            fake_lossB=criterion_D(model_DB(gen_back.detach()),fake)
            real_loss_mid=criterion_D(model_Dmid(midG.detach()),valid_mid)
            fake_loss_mid=criterion_D(model_Dmid(midGB.detach()),fake_mid)
            real_loss_edge=criterion_D(model_DE(targ_edge.detach()),valid)
            fake_loss_edge=criterion_D(model_DE(gen_edge.detach()),fake)

            d_loss=0.25*real_loss+0.25*fake_loss+0.25*real_lossB+0.25*fake_lossB\
                   +0.25*real_loss_mid+0.25*fake_loss_mid+0.25*real_loss_edge+0.25*fake_loss_edge
            d_loss.backward()
            optim_D.step()
            optim_DB.step()
            optim_Dmid.step()
            optim_Dedge.step()

            #log
            running_loss_g_adv +=g_adv.item()*source.size(0)
            running_loss_g_advB +=g_advB.item()*source.size(0)
            running_loss_g_advmid +=g_advmid.item()*source.size(0)
            running_loss_g_advedge +=g_advedge.item()*source.size(0)
            running_loss_g_gen +=g_genB.item()*source.size(0)
            running_loss_g_rec +=g_rec.item()*source.size(0)
            running_loss_g_ind +=g_ind.item()*source.size(0)
            running_loss_d +=d_loss.item()*source.size(0)
            if (batch_idx+1)%50==0:
                current_loss_g_adv=running_loss_g_adv/batch_idx/batch_size
                current_loss_g_advB=running_loss_g_advB/batch_idx/batch_size
                current_loss_g_advmid=running_loss_g_advmid/batch_idx/batch_size
                current_loss_g_advedge=running_loss_g_advedge/batch_idx/batch_size
                current_loss_g_gen=running_loss_g_gen/batch_idx/batch_size
                current_loss_g_rec=running_loss_g_rec/batch_idx/batch_size
                current_loss_g_ind=running_loss_g_ind/batch_idx/batch_size
                current_loss_d=running_loss_d/batch_idx/batch_size
                print('Current loss G (rec/gen/ind): {:.4f}/{:.4f}/{:.4f}, D: {:.4f} and current batch idx {}' \
                      .format(current_loss_g_rec,current_loss_g_gen,current_loss_g_ind,current_loss_d,batch_idx))
                n_it=epoch*len(train_loader)+batch_idx
                writer.add_scalars('data/loss_g_adv',{'forward':current_loss_g_adv,'backward':current_loss_g_advB, 'mid': current_loss_g_advmid, 'edge': current_loss_g_advedge},n_it)
                writer.add_scalars('data/loss_g_gen',{'forward':current_loss_g_rec,'backward':current_loss_g_gen, 'index': current_loss_g_ind},n_it)
                writer.add_scalar('data/loss_d',current_loss_d,n_it)

                each_row=2#2
                x=vutils.make_grid(source.data,nrow=each_row)
                writer.add_image('images/source',x,n_it)
                x=vutils.make_grid(gen.data,nrow=each_row)
                writer.add_image('images/generated',x,n_it)
                x=vutils.make_grid(gen_back.data,nrow=each_row)
                writer.add_image('images/back_generated',x,n_it)
                x=vutils.make_grid(target.data,nrow=each_row)
                writer.add_image('images/target',x,n_it)
                x=vutils.make_grid(gen_edge.data,nrow=each_row)
                writer.add_image('images/gen_edge',x,n_it)
                x=vutils.make_grid(targ_edge.data,nrow=each_row)
                writer.add_image('images/targ_edge',x,n_it)

            del rec, gen, midG, gen_edge, targ_edge, mask

        epoch_loss_g = (running_loss_g_adv+running_loss_g_rec+running_loss_g_gen+running_loss_g_advB+running_loss_g_advmid+running_loss_g_advedge) /dataset_length
        epoch_loss_d = running_loss_d /dataset_length
        print('{} Loss G/D: {:.4f}/{:.4f}'.format('current '+ str(epoch), epoch_loss_g, epoch_loss_d))
        if (epoch + 1) % conf.save_per_epoch == 0:
            save_model(model_G,model_D,model_GB,model_DB,model_Dmid,model_DE,epoch + 1)
    writer.export_scalars_to_json("./result/runs_"+setting+"/all_scalars.json")

def main():
    train()

if(__name__=="__main__"):
    main()



