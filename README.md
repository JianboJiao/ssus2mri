# Self-Supervised Ultrasound to MRI Fetal Brain Image Synthesis

By [Jianbo Jiao](https://jianbojiao.com/), [Ana I.L. Namburete](http://www.ibme.ox.ac.uk/research/biomedia/people/ana-ineda-l-namburete), [Aris T. Papageorghiou](https://www.wrh.ox.ac.uk/team/aris-papageorghiou), [J. Alison Noble](http://www.ibme.ox.ac.uk/research/biomedia/people/professor-alison-noble).


### Introduction

This repository contains the implementation for the method described in the paper "[Self-Supervised Ultrasound to MRI Fetal Brain Image Synthesis](./)". In this work, we propose a self-supervised approach to synthesise MR-like images from unpair ultrasound (US) images.

### Usage

0. This project was implemented with the [PyTorch](https://pytorch.org) framework with [TorchVision](https://pytorch.org/docs/0.2.0/) on Linux with GPUs. Please setup the environment according to the [instructions](https://pytorch.org/get-started/previous-versions/) first.
1. To train the model, please first put your source data (US in this work) into a local folder and set the path to `data_path_train` in [Config.py](Config.py), and your target data (MRI in this work) into another folder with `target_path_train` set to the corresponding path.
2. Then run [Train.py](Train.py) to train the model.
3. Result and trained models will be stored under folder "result".	

### Citation

If you find the code useful, please consider citing:

	@InProceedings{SSUS2MRI20,
		author = {Jianbo Jiao, Ana I.L. Namburete, Aris T. Papageorghiou, J. Alison Noble},
		title = {Self-Supervised Ultrasound to MRI Fetal Brain Image Synthesis},
		booktitle = {IEEE Transactions on Medical Imaging},
		year = {2020}
	}
	