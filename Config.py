class Config:
    data_path_train = './path/to/US/images'#training data path for US images
    target_path_train = './path/to/MR/images'#training data path for MR images
    data_path_checkpoint ='./result'

    img_channel = 3
    max_epoch = 100
    crop_img_size = 256
    learning_rate = 1e-4
    save_per_epoch = 10
    cuda = "cuda:0"
